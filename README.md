A small utility to read the marks from [Sysprof](https://gitlab.gnome.org/GNOME/sysprof) capture files and convert them to JSON format. Useful for showing the mark data in a different formats than Sysprof GUI and doing fancy things like automating the analyzing of results.

### Usage

`sysprof-marks-to-json capture.syscap > marks.json`
