/* main.c
 *
 * Copyright 2022 Jonas Dreßler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include <glib.h>
#include <stdlib.h>
#include <stdio.h>
#include <sysprof-4/sysprof-capture.h>
#include <gio/gio.h>

typedef struct
{
  gchar    name[152];
  guint64  count;
  gint64   max;
  gint64   min;
  gint64   avg;
  guint64  avg_count;
} SysprofMarkStat;

gint
main (gint   argc,
      gchar *argv[])
{
  SysprofCaptureReader *reader;
  SysprofCaptureFrame frame;
  g_autoptr(GHashTable) mark_stats = NULL;
  g_autoptr(GArray) marks = NULL;
  GHashTableIter iter;
  gpointer k,v;
  gboolean first = TRUE;

  if (argc != 2)
    {
      g_warning ("A sysprof capture file has to be supplied");
      return EXIT_FAILURE;
    }

  reader = sysprof_capture_reader_new (argv[1]);
  if (reader == NULL)
    {
      g_warning ("Failed to open capture");
      return EXIT_FAILURE;
    }

  mark_stats = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);
  marks = g_array_new (FALSE, FALSE, sizeof (SysprofMarkStat));

  while (sysprof_capture_reader_peek_frame (reader, &frame))
    {
      if (frame.type == SYSPROF_CAPTURE_FRAME_MARK)
        {
          const SysprofCaptureMark *mark;

          if ((mark = sysprof_capture_reader_read_mark (reader)))
            {
              SysprofMarkStat *mstat;
              gchar name[152];
              gpointer idx;

              g_snprintf (name, sizeof name, "%s:%s", mark->group, mark->name);

              if (!(idx = g_hash_table_lookup (mark_stats, name)))
                {
                  SysprofMarkStat empty = {{0}};

                  g_strlcpy (empty.name, name, sizeof empty.name);
                  g_array_append_val (marks, empty);
                  idx = GUINT_TO_POINTER (marks->len);
                  g_hash_table_insert (mark_stats, g_strdup (name), idx);
                }

              mstat = &g_array_index (marks, SysprofMarkStat, GPOINTER_TO_UINT (idx) - 1);

              if (mark->duration > 0)
                {
                  if (mstat->min == 0 || mark->duration < mstat->min)
                    mstat->min = mark->duration;
                }

              if (mark->duration > mstat->max)
                mstat->max = mark->duration;

              if (mark->duration > 0)
                {
                  mstat->avg += mark->duration;
                  mstat->avg_count++;
                }
              mstat->count++;
            }
        }
      else
        {
          sysprof_capture_reader_skip (reader);
        }
    }

  printf ("{");

  g_hash_table_iter_init (&iter, mark_stats);
  while (g_hash_table_iter_next (&iter, &k, &v))
    {
      guint idx = GPOINTER_TO_UINT (v) - 1;
      SysprofMarkStat *mstat = &g_array_index (marks, SysprofMarkStat, idx);

      if (mstat->avg_count > 0 && mstat->avg > 0)
        mstat->avg /= mstat->avg_count;

      if (!first)
        printf (",\n");

      printf ("\"%s\": { \"count\": %ld, \"avg\": %f, \"min\": %f, \"max\": %f }",
              (gchar *)k, mstat->count, mstat->avg / 1000000.0, mstat->min / 1000000.0, mstat->max / 1000000.0);

      first = FALSE;
    }

  printf ("}\n");

  sysprof_capture_reader_reset (reader);
  sysprof_capture_reader_unref (reader);

	return EXIT_SUCCESS;
}
